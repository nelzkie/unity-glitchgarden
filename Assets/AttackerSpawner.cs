﻿using UnityEngine;
using System.Collections;

public class AttackerSpawner : MonoBehaviour {

    public GameObject[] attackerArray;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    foreach (GameObject thisAttacker in attackerArray)
	    {
            if (isTimeToSpawn(thisAttacker))
            {
                Spawn(thisAttacker);
            }
	    }
	}

    private void Spawn(GameObject thisAttacker)
    {
        GameObject enemy = Instantiate(thisAttacker);
        enemy.transform.parent = transform;
        enemy.transform.position = transform.position;
    }

    private bool isTimeToSpawn(GameObject thisAttacker)
    {
        Attacker attacker = thisAttacker.GetComponent<Attacker>();
        float spawnDelay = attacker.spawnEverySeconds;

        float spawnPerSecond = 1 / spawnDelay;

        if(Time.deltaTime > spawnDelay)
        {
          Debug.LogWarning("Spawn rate capped by frame rate");
        }

        float threshold = spawnPerSecond * Time.deltaTime / 5;
        if (Random.value < threshold)
            return true;

        return false;
    }
}

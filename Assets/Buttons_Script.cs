﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Buttons_Script : MonoBehaviour {

    public GameObject defenderPrefab;
    private Buttons_Script[] buttonArray;
    public static GameObject selectedDefender;
    StarDisplay starDisplay;
    Text cost;

	// Use this for initialization
	void Start () {

        starDisplay = FindObjectOfType<StarDisplay>();

        cost = GetComponentInChildren<Text>();

        if (!cost)
        {
            Debug.LogWarning("No cost Text");
        }
        cost.text = defenderPrefab.GetComponent<Defender>().starCost.ToString();
   
   
        //You can do this
        // buttonArray = GameObject.FindGameObjectsWithTag("Buttons") ;

        //Or
        buttonArray = FindObjectsOfType<Buttons_Script>();
	    foreach (var button in buttonArray)
	    {
            button.GetComponent<SpriteRenderer>().color = Color.black; 

        }
      
    }
	
	// Update is called once per frame
	void Update () {
        //foreach (var button in buttonArray)
        //{
        //    if(starDisplay.getStars > cost.starCost)
        //    {
        //        button.GetComponent<SpriteRenderer>().color = Color.yellow;
        //    }
        //    else
        //    {
        //        button.GetComponent<SpriteRenderer>().color = Color.black;
        //    }
           

        //}
    }
    void OnMouseDown()
    {

        foreach (var button in buttonArray)
        {
            button.GetComponent<SpriteRenderer>().color = Color.black;
        }

        GetComponent<SpriteRenderer>().color = Color.white;
        selectedDefender = defenderPrefab;
     
    }

    void OnMouseEnter()
    {
        //foreach (var button in buttonArray)
        //{
        //    button.GetComponent<SpriteRenderer>().color = Color.black;
        //}

        //GetComponent<SpriteRenderer>().color = Color.white;
        //selectedDefender = defenderPrefab;
    }


}

﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

    private GameObject defenderParent;
    public Camera myCamera;

    private StarDisplay starDisplay;
	// Use this for initialization
	void Start () {
	    defenderParent = GameObject.Find("Defender");
        if (!defenderParent)
        {
            defenderParent = new GameObject("Defender");
        }

        starDisplay = GameObject.FindObjectOfType<StarDisplay>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        Vector2 rawPos = CalculateWorldPointOfMouseClick();
        Vector2 roundedOfPos = SnapToGrid(rawPos);

        GameObject defender = Buttons_Script.selectedDefender;

        int defendersCost = defender.GetComponent<Defender>().starCost;

        if(starDisplay.UseStars(defendersCost) == StarDisplay.Status.SUCCESS)
        {
            SpawnDefender(roundedOfPos);
        }
       
       
    }

    private void SpawnDefender(Vector2 roundedOfPos)
    {
        GameObject newGameObject =
            Instantiate(Buttons_Script.selectedDefender, roundedOfPos, Quaternion.identity) as GameObject;
        newGameObject.transform.parent = defenderParent.transform;
    }


    // this will place the defenders on the exact tile location
    Vector2 SnapToGrid(Vector2 RawWorldPosition)
    {
        float newX = Mathf.RoundToInt(RawWorldPosition.x);
        float newY = Mathf.RoundToInt(RawWorldPosition.y);
        return new Vector2(newX, newY);
    }
    Vector2 CalculateWorldPointOfMouseClick()
    {
        float mouseX = Input.mousePosition.x;
        float mouseY = Input.mousePosition.y;
        float distranceFromCamera = 10; // distance doesnt matter in 2D anyway so its random value in here

        Vector3 weirdTriplet = new Vector3(mouseX,mouseY,distranceFromCamera);

        Vector2 worldPos = myCamera.ScreenToWorldPoint(weirdTriplet);

        return worldPos;
    }
}

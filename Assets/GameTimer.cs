﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

    public float levelSeconds = 100;

    bool isEndOfLevel;
    AudioSource audioSource;
    Slider slider;
    public int secondsLeft;
    LevelManager levelManager;
    GameObject winLable;
    
	// Use this for initialization
	void Start () {
        slider = GetComponent<Slider>();
        slider.value = 0;

        audioSource = GetComponent<AudioSource>();

        levelManager = FindObjectOfType<LevelManager>();
        winLable = GameObject.Find("Win Condition");
        winLable.SetActive(false);

       

    }
	
	// Update is called once per frame
	void Update () {
        slider.value =  (Time.timeSinceLevelLoad / levelSeconds);

        if(Time.timeSinceLevelLoad >= levelSeconds && !isEndOfLevel)
        {
            HandleWinCondition();
        }
	}

    void DestroyObjectsOnWin()
    {
        GameObject[] gameObjectsarray = GameObject.FindGameObjectsWithTag("DestroyOnWin");
        foreach (GameObject targetGameObject in gameObjectsarray)
        {
            
            Destroy(targetGameObject);
        }
    }

    void HandleWinCondition()
    {
        DestroyObjectsOnWin();
        winLable.SetActive(true);
        audioSource.Play();
        Invoke("LoadNextLevel", audioSource.clip.length);
        isEndOfLevel = true;
    }

    void LoadNextLevel()
    {
        levelManager.loadNextLevel();
    }
}

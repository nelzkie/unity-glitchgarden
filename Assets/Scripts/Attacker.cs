﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Attacker : MonoBehaviour {

    [Range(-1f, 1.5f)] // This is called annotation
    public float currentSpeed;

    [Tooltip("Average number of seconds to spawn")]
    public float spawnEverySeconds;
    private Animator animator;

    private GameObject currentTraget;

	// Use this for initialization
	void Start () {
        //Rigidbody2D myRigidbody2D = gameObject.AddComponent<Rigidbody2D>();
        //myRigidbody2D.isKinematic = true;


        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	    transform.Translate(Vector3.left * currentSpeed * Time.deltaTime); // move the attacker
        if (!currentTraget)
        {
            animator.SetBool("isAttacking", false);
        }

        if (animator.GetBool("isAttacking"))
        {
            SetSpeed(0);
        }
    
	}

    void OnTriggerEnter2D(Collider2D col)
    {
      
    }

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }


    // Called from the animator at the time of the actual attack
    void StrikeCurrentTarget(float damage)
    {
        

        if (currentTraget)
        {
            Health health = currentTraget.GetComponent<Health>();
            if (health)
            {
                health.DealDamage(damage);
            }
        }

  

    }

    public void Attack ( GameObject obj)
    {
        currentTraget = obj;
                
    }
}

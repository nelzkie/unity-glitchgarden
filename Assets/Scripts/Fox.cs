﻿using UnityEngine;
using System.Collections;
using JetBrains.Annotations;

[RequireComponent(typeof(Attacker))]
public class Fox : MonoBehaviour {
    private Animator animator;
    Attacker attacker;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        attacker = GetComponent<Attacker>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("Fox collided with  " + collider);
        GameObject obj = collider.gameObject;

        // Leave if not colliding with the defenders
        if (!obj.GetComponent<Defender>())
        {
            return; 
        }

        if (obj.GetComponent<Stone>())
        {
            animator.SetTrigger("JumpTrigger");
        }
        else
        {
            animator.SetBool("isAttacking",true);
            attacker.Attack(obj);
        }
    }
}

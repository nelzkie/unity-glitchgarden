﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public float health;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	     
	}

    public void DealDamage(float damage)
    {
        if((health -= damage) > 0)
        {
            
        }

        if(health < 0)
        {
            // Optionally trigger an animation but we dont have any animation for die state
            DestroyObject();
        }
    }

    public void DestroyObject()
    {
        Destroy(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
    public float autoLoadNextLevelAfter; // this is the offset to show the next level
    void Start()
    {
        if(autoLoadNextLevelAfter <= 0)
        {
            Debug.LogWarning("use positive number in Load Level After");
        }
        else
        {
            Invoke("loadNextLevel", autoLoadNextLevelAfter);
        }
        
    }
    public void LoadLevel(string name)
    {
        Debug.Log("new level load " + name);

     
        Application.LoadLevel(name);
    }

    public void QuitLevel()
    {
        Debug.Log("quit");
        Application.Quit();
    }

    public void loadNextLevel()
    {
       
        Application.LoadLevel(Application.loadedLevel + 1);
    }


    void Update()
    {
        
        if (Input.GetKey(KeyCode.A))
        {

            QuitLevel();
        }
    }

  

}

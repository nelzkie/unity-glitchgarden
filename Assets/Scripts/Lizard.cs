﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Attacker))]
public class Lizard : MonoBehaviour {

    private Animator animator;
    Attacker attacker;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        attacker = GetComponent<Attacker>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
      
        GameObject obj = collider.gameObject;

        // Leave if not colliding with the defenders
        if (!obj.GetComponent<Defender>())
        {
          
            return;
        }
       
            
        animator.SetBool("isAttacking", true);
        if (animator.GetBool("isAttacking"))
        {
            attacker.SetSpeed(0);
        }

        attacker.Attack(obj);

    }
}

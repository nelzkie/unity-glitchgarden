﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
    private AudioSource audioSource;
    public AudioClip[] LevelMusicChangeAudioClips;
   
    // Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>(); // get the audio source attach to the game object
        audioSource.volume = PlayerPrefsManager.GetMasterVolume();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Check the execution order of unity
    void OnLevelWasLoaded(int level)
    {
        Debug.Log(level);
        Debug.Log(LevelMusicChangeAudioClips[level]);

        AudioClip thisLevelMusic = LevelMusicChangeAudioClips[level];

        if (thisLevelMusic) // if there is a music attach
        {
            audioSource.clip = thisLevelMusic;
            audioSource.loop = true;
            audioSource.Play();
        }
    }

    public void SetVolume(float volume)
    {
        audioSource.volume = volume;
    }
}

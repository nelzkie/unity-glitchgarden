﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour {


    const string MASTER_VOLUME_KEY = "master_volume";
    const string DIFFICULTY_KEY = "difficulty";
    const string LEVEL_KEY = "level_unlocked_2";

    public static void SetMasterVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Master Volume out of range");
        }

    }

    public static float GetMasterVolume()
    {
        return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
    }

    public static void UnLockLevel(int level)
    {
        if(level <= SceneManager.sceneCountInBuildSettings - 1) // the scene we build start from 0 index
        {
            PlayerPrefs.SetInt(LEVEL_KEY + level.ToString(),1); // use 1 for true
        }
        else
        {
            Debug.LogError("Trying to unlock leve not in build order");
        }
    }


    public static bool IsLevelUnlocked(int level)
    {
        int levelValue = PlayerPrefs.GetInt(LEVEL_KEY + level.ToString());
        bool isLevelUnlocked = (levelValue == 1);
        if(level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            return isLevelUnlocked;
        }
        else
        {
            Debug.LogError("Trying to query level not in build order");
            return false;
        }
    }


    public static void SetDifficulty(float difficulty)
    {
        if (difficulty >= 0f && difficulty <= 1)
        {
            PlayerPrefs.SetFloat(DIFFICULTY_KEY,difficulty);
        }
        else
        {
            Debug.LogError("Difficulty out of rannge");
        }
    }

    public static float GetDifficulty()
    {
        return PlayerPrefs.GetFloat(DIFFICULTY_KEY);
    }
}

﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {


    public float speed;
    public float damage;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	   transform.Translate(Vector3.right * speed * Time.deltaTime);
        Destroy(gameObject,1);
	}

    void OnTriggerEnter2D(Collider2D collider2D)
    {

        
        Attacker attacker = collider2D.gameObject.GetComponent<Attacker>();
        Health health = collider2D.gameObject.GetComponent<Health>(); 
        print(attacker + "   " + health);
        if(attacker && health)
        {
            health.DealDamage(damage);
            
        }
        Destroy(gameObject);
    }
}

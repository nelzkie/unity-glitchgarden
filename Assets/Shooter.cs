﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    public GameObject projectile,gun;
    GameObject projectileParent;
    Animator animator;
    private AttackerSpawner mylanAttackerSpawner;
    // Use this for initialization

    
    void Start () {
     
        projectileParent = GameObject.Find("Projectiles");
        if (!projectileParent)
        {
            projectileParent = new GameObject("Projectile");    // Create a blank game object via script
        }

        animator = GetComponent<Animator>();
        SetMyLaneSpawner();


    }
	
	// Update is called once per frame
	void Update () {
        if (isAttackerAheadinLane())
        {
            animator.SetBool("isAttacking",true);
        }
        else
        {
            animator.SetBool("isAttacking", false);
        }
	}


    // this is to find if there is an attacker spawned to the lane
    void SetMyLaneSpawner()
    {
        AttackerSpawner[] spawners = GameObject.FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner attackerSpawner in spawners)
        {
            if(attackerSpawner.transform.position.y == transform.position.y)
            {
                mylanAttackerSpawner = attackerSpawner;
                return;
            }
        }

        Debug.LogError(name + " is not found");
    }

    private bool isAttackerAheadinLane()
    {

        // if no attackers in lane
        if(mylanAttackerSpawner.transform.childCount <= 0)
        {
            return false;
        }

        foreach ( Transform attackers in mylanAttackerSpawner.transform)
        {
            Debug.Log(attackers.name);
            if(attackers.transform.position.x > transform.position.x)
            {
                return true;
            }
        }

        // attackers in lane but behind us
        return false;
        

       
    }

    void Fire()
    {
        GameObject newprojectile = Instantiate(projectile) as GameObject;
        newprojectile.transform.parent = projectileParent.transform;
        newprojectile.transform.position = gun.transform.position;
    }
}
 
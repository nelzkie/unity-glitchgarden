﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(Text))]
public class StarDisplay : MonoBehaviour {

    Text starText;
    int stars = 100;

    public int getStars
    {
        get { return stars; }
    }
    public enum Status
    {
        SUCCESS,
        FAILURE
    }
    // Use this for initialization
    void Start () {

        starText = GetComponent<Text>();
        stars = 100;
        UpdateDisplay();
    }
	
	// Update is called once per frame


    public void AddStars(int amount)
    {
        stars += amount;
        UpdateDisplay();
    }

    public Status UseStars(int amount)
    {
        if(stars >= amount)
        {
            stars -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        }

       return Status.FAILURE;

    }

    private void UpdateDisplay()
    {
        starText.text = stars.ToString();
    }
}
